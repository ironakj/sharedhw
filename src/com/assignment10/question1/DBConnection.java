package com.assignment10.question1;

import java.sql.*;

public class DBConnection {
		Connection con=null;
		Statement st=null;
		ResultSet rs=null;
		Statement pre_st=null;
		
		public Connection getCon() throws SQLException, ClassNotFoundException{
				Class.forName("com.mysql.jdbc.Driver");
				con = DriverManager.getConnection("jdbc:mysql://127.0.0.1/assignment","root","root");
				return con;
		}
		
		public ResultSet getResultSet(String query) throws SQLException, ClassNotFoundException{
			try {
				con=this.getCon();
				st=con.createStatement();
				rs=st.executeQuery(query);
			}catch (SQLException e) {
				e.printStackTrace();
			}
			return rs;
		}
		

		@Override
		protected void finalize() throws Throwable {
			rs.close();
			st.close();
			pre_st.close();
			con.close();
			super.finalize();
		}
}
