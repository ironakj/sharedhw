package com.assignment10.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CallProcedureTest.class, DBConnectionTest.class,GenericDisplayTest.class })
public class AllTests {
	
}
