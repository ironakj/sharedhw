package com.assignment10.tests;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Before;
import org.junit.Test;

import com.assignment10.question3.GenericDisplay;

public class GenericDisplayTest {
	GenericDisplay generic;
	
	@Before
	public void setUp() throws Exception {
		generic=new GenericDisplay();
	}

	@Test
	public void testDisplayTable() throws ClassNotFoundException, SQLException {
		assertTrue(generic.displayTable("STUDENT"));
		assertTrue(generic.displayTable("EMPLOYEE"));
	}

}
