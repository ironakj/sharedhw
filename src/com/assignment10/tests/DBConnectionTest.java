package com.assignment10.tests;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.assignment10.question1.DBConnection;
import com.mysql.jdbc.PreparedStatement;

public class DBConnectionTest {
	PreparedStatement preparedStatement1=null;
	PreparedStatement preparedStatement2=null;
	Connection conn=null;
	DBConnection db;
	String resultSetQuery;
	String insertQuery;
	String updateQuery;
	ResultSet result;
	int fetched;
	@Before
	public void setUp() throws Exception {
		db=new DBConnection();
		conn=db.getCon();
		resultSetQuery="SELECT * FROM EMPLOYEE";
		insertQuery="INSERT INTO EMPLOYEE VALUES(?,?,?)";
		updateQuery="UPDATE Employee SET dept_id=? WHERE id=?";
		result=db.getResultSet(resultSetQuery);
		result.last();
		fetched=result.getRow();
		
		preparedStatement1=(PreparedStatement) conn.prepareStatement(insertQuery);
		preparedStatement1.setInt(1, 110);
		preparedStatement1.setInt(2, 102);
		preparedStatement1.setString(3, "Test Case Employee");
		
		preparedStatement2=(PreparedStatement) conn.prepareStatement(updateQuery);
		preparedStatement2.setInt(1, 12);
		preparedStatement2.setInt(2, 1);
	}

	@Test
	public void testgetCon() throws ClassNotFoundException, SQLException {
		assertTrue(conn.isValid(10));
	}
	
	@Test
	public void testgetResultSet(){
		assertEquals(5,fetched);	
	}
	@Test
	public void testUpdate() throws SQLException, ClassNotFoundException{
		preparedStatement2.execute();
		result=db.getResultSet("select * from employee where id=1");
		result.next();
		assertEquals(12,result.getInt(2));	
	}
	
	@Test
	public void testPreparedStatement() throws SQLException, ClassNotFoundException{
		/*
		 * Test if new row is added
		 * 
		 * */
		preparedStatement1.execute();
		result=db.getResultSet(resultSetQuery);
		result.last();
		fetched=result.getRow();
		assertEquals(6,fetched);
	}

	@After
	public void set() throws SQLException{
		/* Reverting back changes after test */
		updateQuery="UPDATE Employee SET dept_id=8 WHERE id=1";
		resultSetQuery="DELETE FROM EMPLOYEE where id=110";
		preparedStatement1=(PreparedStatement) conn.prepareStatement(resultSetQuery);
		preparedStatement2=(PreparedStatement) conn.prepareStatement(updateQuery);
		preparedStatement1.execute();
		preparedStatement2.execute();
	}
}
