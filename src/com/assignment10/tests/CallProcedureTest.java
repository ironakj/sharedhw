package com.assignment10.tests;

import static org.junit.Assert.*;

import java.sql.*;

import org.junit.Before;
import org.junit.Test;

import com.assignment10.question1.DBConnection;

public class CallProcedureTest {
	CallableStatement callableStatement = null;
	Connection conn=null;
	DBConnection db;
	String resultSetQuery;
	ResultSet old;
	ResultSet result;
	@Before
	public void setUp() throws Exception {
		db=new DBConnection();
		conn=db.getCon();
		resultSetQuery="SELECT basic_salary,hra FROM EMPLOYEE_SALARIES";
		old=db.getResultSet(resultSetQuery);
		callableStatement = conn.prepareCall("call hike()");
	}
	
	// Test will check if calling the procedure actually increases basic salaries

	@Test
	public void testExecuteProcedure() throws SQLException, ClassNotFoundException {
		callableStatement.execute();
		result=db.getResultSet(resultSetQuery);
		
		// & will call both sides to execute
		while(old.next() & result.next()) {
			assertEquals((old.getInt(1)+0.1*old.getInt(2)), result.getInt(1),0);
			// Explicitly increasing old values to check
			// if values were actually increased, test should run fine
		}
	}

}
