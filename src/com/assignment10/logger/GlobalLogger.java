package com.assignment10.logger;

import org.apache.log4j.Logger;

public abstract class GlobalLogger {
	public final static Logger logger = Logger.getLogger(GlobalLogger.class.getName());
}
