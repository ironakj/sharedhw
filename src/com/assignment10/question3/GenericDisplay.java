package com.assignment10.question3;

import java.sql.*;

import com.assignment10.question1.DBConnection;
import static com.assignment10.logger.GlobalLogger.logger;

public class GenericDisplay {
	DBConnection dbcon=new DBConnection();
	Connection con;
	ResultSet result;
	ResultSetMetaData metaData;
	int fetch;
	String query="select * from $table";
	public boolean displayTable(String tableName) throws SQLException, ClassNotFoundException{
		con=dbcon.getCon();
		query=query.replace("$table", tableName);
		result=dbcon.getResultSet(query);
		logger.info(query);
		metaData= result.getMetaData();
		result.last();
		fetch=result.getRow();
		logger.info("\t"+metaData.getColumnLabel(1)+"\t"+metaData.getColumnLabel(2)+"\t"+metaData.getColumnLabel(3));
		while(result.next()){
			logger.info("\t"+result.getInt(1)+"\t"+result.getInt(2)+"\t"+result.getString(3));
		}
		return fetch!=0;
	}
	@Override
	protected void finalize() throws Throwable {
		con.close();
		result.close();
		super.finalize();
	}
}
